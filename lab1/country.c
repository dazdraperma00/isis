/*
 *  Задание #6
 *  Автор: Ханин Александр ИУ3-82
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

int main(int argc, char * argv[])
{
    COUNTRY * list;

    /* Загрузка списка */
    list = load();

    /* СДЕЛАТЬ: 
     * добавить обработку команд из командной строки 
     * if (strcmp(argv[1], "add") == 0) { ... }
     * else if ( ....
     */
    int err = 0;

    if (argc < 2) {
        printf("format: country <command> <args>\n");
        err = 1;
    }
    else {
        // ADD
        if (!strcmp(argv[1], "add")) {
            if (argc != 5) {
                printf("format: country add <name> <population> <area>\n");
                err = 1;
            }
            else {
                err = add(&list, argv[2], atoi(argv[3]), atoi(argv[4]));
                if (err != 0) {
                    printf("command failed\n");
                }
            }
        }
        // DELETE
        else if (!strcmp(argv[1], "delete")) {
            if (argc != 3) {
                printf("format: country delete <name>\n");
                err = 1;
            }
            else {
                COUNTRY* p = find(list, argv[2]);
                if (!p) {
                    printf("country doesn't exists\n");
                    err = 1;
                }
                else {
                    delete(&list, p);
                }
            }
        }
        // DUMP
        else if (!strcmp(argv[1], "dump")) {
            if (argc > 3) {
                printf("format: country dump <key>\n");
                err = 1;
            }
            else {
                if (argc == 2) {
                    dump(list);
                }
                else {
                    if (!strcmp(argv[2], "-n")) {
                        err = sort_by_name(&list);
                        dump(list);
                        if (err != 0) {
                            printf("command failed\n");
                        }
                    }
                    else if (!strcmp(argv[2], "-a")) {
                        err = sort_by_area(&list);
                        dump(list);
                        if (err != 0) {
                            printf("command failed\n");
                        }
                    }
                    else if (!strcmp(argv[2], "-p")) {
                        err = sort_by_population(&list);
                        dump(list);
                        if (err != 0) {
                            printf("command failed\n");
                        }
                    }
                    else {
                        printf("format: country dump <key>\n");
                        err = 1;
                    }
                }
            }
        }
        // VIEW
        else if (!strcmp(argv[1], "view")) {
            if (argc != 3) {
                printf("format: country view <name>\n");
                err = 1;
            }
            else {
                COUNTRY* p = find(list, argv[2]);
                if (!p) {
                    printf("country doesn't exists\n");
                    err = 1;
                }
                else {
                    print_country(p);
                }
            }
        }
        // COUNT
        else if (!strcmp(argv[1], "count")) {
            if (argc != 2) {
                printf("format: country count\n");
                err = 1;
            }
            else {
                printf("%d\n", count(list));
            }
        }
        // DEFAULT
        else {
            printf("command not found\n");
            return 1;
        }
    }
    
    save(list);



    /* Удаление списка из динамической памяти */
    clear(list);

    return 0;
}
