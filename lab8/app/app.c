#include <stdio.h>
#include <fcntl.h>
#include <string.h>

#define device_name "/dev/hello"
#define file_name "/usr/share/misc/pci.ids"

#define BUF_SIZE 1024
char buf[BUF_SIZE];
FILE* f;

void print_vendor_info(const unsigned short vendor)
{
    char str[200];
    while (fgets(str, 100, f)) {
        if (str[0] != '#' && str[0] != '\t') {
            unsigned short _vendor;
            char vendorStr[100];
            sscanf(str, "%hx %[^\t\n]", &_vendor, vendorStr);

            if (_vendor == vendor) {
                printf("%s ", vendorStr);
                return;
            }
        }
    }

    printf("vendor not found\n");
}

void print_device_info(const unsigned short device)
{
    char str[200];
    while (fgets(str, 100, f)) {
        if (str[0] == '\t' && str[1] != '\t') {
            unsigned short _device;
            char deviceStr[100];
            sscanf(str, "%hx %[^\t\n]", &_device, deviceStr);
           
            if (_device == device) {
                printf("%s ", deviceStr);
                return;
            }
        }
    }

    printf("device not found\n");
}

void print_pci_info(void)
{
    char* token;
    char* sep = "\n";
    token = strtok(buf, sep);

    while (token) {
        unsigned short vendor;
        unsigned short device;
        char addr[100];
        sscanf(token, "%hu %hu %s", &vendor, &device, addr);
        print_vendor_info(vendor);
        print_device_info(device);
        fseek(f, 0, SEEK_SET);
        printf("%s\n", addr);
        token = strtok(NULL, sep);
    }
}

int main(int argc, char * argv[])
{
    const int d = open(device_name, O_RDWR);
    if (d == -1) {
        printf("device opening error\n");
        return -1;
    }
    
    if (ioctl(d, 0, buf)) {
        printf("device read error\n");
        return -1;
    }   

    close(d);

    f = fopen(file_name, "r");
    if (!f) {
        printf("file opening error\n");
        return -1;
    }

    print_pci_info();

    fclose(f);

    return 0;
}