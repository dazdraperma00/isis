#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define GET 0
#define POST 1


int Connect()
{
    struct hostent* hp = gethostbyname("www.iu3.bmstu.ru");
    if (hp == 0) {
        printf("ERROR: Can't get server host\n");
        return -1;
    }

    struct sockaddr_in ssin;

    memcpy(&ssin.sin_addr, hp->h_addr_list[0], hp->h_length);
    ssin.sin_family = hp->h_addrtype;
    ssin.sin_port = htons(8090);

    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        printf("ERROR: Error with socket creation\n");

        return -1;
    }

    if (connect(sock, &ssin, sizeof(struct sockaddr_in)) == -1) {
        printf("ERROR: Can't connect to server\n");
        return -1;
    }

    return sock;
}

int Request(const int s, char* buf, const int size, const int req, char* type, char* format)
{
    int len;
    if (req == GET) {
        len = sprintf(buf, "GET /WebApi/time?type=%s&format=%s HTTP/1.0\n\n", type, format);
    }
    else if (req == POST) {
        len = sprintf(buf, "POST /WebApi/time HTTP/1.0\nContent-Type:application/x-www-form-urlencoded\nContent-Length:20\n\ntype=%s&format=%s\n", type, format);
    }

    if (send(s, buf, len, 0) == -1) {
        printf("ERROR: Error with request sending\n");
        return -1;
    }

    len = 0;
    int i = 0;
    do {
        i = recv(s, buf + len, size, 0);
        if (i == -1) {
            printf("ERROR: Error with response receiving\n");
            return -1;
        }

        len += i;
    } while (i != 0);

    buf[len] = '\0';
}

int GetHTTPCode(char* buf)
{
    const int codePos = 9;
    char code[4];

    code[3] = '\0';
    memcpy(code, buf + codePos, 3);
    return atoi(code);
}

char* GetHTTPContent(char* buf)
{
    for (int i = 0; buf[i] != '\0'; ++i){
        if ((buf[i] == '\n' || buf[i] == '\r') && (buf[++i] == '\n' || buf[i] == '\r')) {
            return buf + i;
        }
    }
    return 0;
}

int main(int argc, char** argv)
{
    char* type = "utc";
    char* format = "unix";
    int req = GET;

    for (int i = 1; i < argc; ++i) {
        if (!strcmp(argv[i], "-t")) {
            type = argv[++i];
        }
        else if (!strcmp(argv[i], "-f")) {
            format = argv[++i];
        }
        else if (!strcmp(argv[i], "-post")) {
            req = POST;
        }
        else if (!strcmp(argv[i], "-get")) {
            req = GET;
        }
        else {
            printf("ERROR: Key does not recognized\n");
            return 1;
        }
    }

    const int s = Connect();
    if (s == -1) {
        return 1;
    }

    const int bufSize = 500;
    char buf[bufSize];
    if (Request(s, buf, bufSize, req, type, format) == -1) {
        return 1;
    }

    const int code = GetHTTPCode(buf);
    if (code != 200) {
        printf("ERROR: HTTP error code - %d \n", code);
        return 1;
    }

    char* res = GetHTTPContent(buf);
    if (res != 0) {
        printf("%s\n", res);
    }
    else {
        printf("empty\n");
    }

    return 0;
}
